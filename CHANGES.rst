=======
CHANGES
=======

Development
===========

* Fixed date breadcrumbs showing in all pages.

Version 0.1.0 (2016-02-28T02:54:05Z)
====================================

* First release.
